package com.osthus.shapes.shaclc;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

import com.google.inject.Inject;
import com.google.inject.Singleton;
@Singleton
public class VocabularyProvider implements IVocabularyProvider {
	private static final Logger log = Logger.getLogger(VocabularyProvider.class);
	@Inject
	ICatalogProvider catalogProvider;

	private Map<String, IVocabulary> vocabularies = new HashMap<>();


	@Override
	public IVocabulary getVocabulary(Resource resource) {
		URI resourceURI = resource.getURI();
		IVocabulary vocab = vocabularies.get(resourceURI.toString());
		if (vocab == null) {
			URI folder = resource.getURI().trimSegments(1) ;
			ICatalog catalog = null;
			if (catalogProvider != null) {
				catalog = catalogProvider.getCatalog(resource);
			} else {
				log.warn("No catalog found");
				catalog = new NoopCatalog();
			}
			vocab = new Vocabulary(resource, folder, catalog);
			vocabularies.put(resourceURI.toString(), vocab);
		}
		return vocab;
	}
}
