package com.osthus.shapes.shaclc;

import java.util.Set;

public interface IVocabulary {

	/**
	 * Load ontology and add the labels to know terms
	 * @param url
	 *
	 * @return true if added, false if already imported
	 */
 	boolean addOntology(String url);

 	/**
 	 * Register prefix for use in prefixed labels
 	 * @param prefix
 	 * @param ns
 	 *
	 * @return true if added, false if already imported
 	 */
	boolean addPrefix(String prefix, String ns);

	Set<String> prefixes();
	Set<String> ontologies();
	Set<String> labelsForNamespace(String ns);

	Set<String> labelsForPrefix(String prefix);

	/**
	 *
	 * @param pLabel
	 * @return iri if pLabel exists in imported ontologies, null otherwise
	 */
	String iriForPrefixedLabel(String pLabel);

	String iriOfPrefixLabel(String prefix, String label);

	String iriOfNamespaceLabel(String ns, String label);

	String iriOfNamespaceLabel(String ns, String label, String prefix);

	/**
	 * Force a reload of all added ontologies
	 */
	void reload();
}
