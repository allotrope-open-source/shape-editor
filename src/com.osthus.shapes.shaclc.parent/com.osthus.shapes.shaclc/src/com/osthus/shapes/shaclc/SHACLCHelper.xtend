package com.osthus.shapes.shaclc

import com.osthus.shapes.shaclc.sHACLC.BaseDecl
import com.osthus.shapes.shaclc.sHACLC.PrefixDecl
import java.util.HashSet
import java.util.Set
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import com.osthus.shapes.shaclc.sHACLC.ImportsDecl
import java.util.Map
import java.util.HashMap

class SHACLCHelper {
	static public def URI getBase(Resource resource) {
		val bdecl = resource.allContents.toIterable.filter(typeof(BaseDecl)).head;
		return URI.createURI(bdecl.iri)
	}

	static public def Map<String, String> getPrefixes(Resource resource) {
		val HashMap<String, String> prefixes = new HashMap<String, String>();
		for (pdecl : resource.allContents.toIterable.filter(typeof(PrefixDecl))) {
			prefixes.put(pdecl.prefix.substring(0,pdecl.prefix.length-1), stripQuotes(pdecl.iri))
		}
		prefixes
	}

	static public def Set<URI> getImports(Resource resource) {
		val HashSet<URI> imports = new HashSet<URI>();
		for (idecl : resource.allContents.toIterable.filter(typeof(ImportsDecl))) {
			imports.add(URI.createURI(stripQuotes(idecl.iri)))
		}
		imports
	}

	static public def String stripQuotes(String str) {
		if (str.startsWith("\"\"\"") || str.startsWith("\'\'\'")) {
			return str.substring(3, str.length() - 3);
		} else {
			if (str.startsWith("\"") || str.startsWith("\'") || str.startsWith("<"))
				return str.substring(1, str.length() - 1);
		}
	}

}
