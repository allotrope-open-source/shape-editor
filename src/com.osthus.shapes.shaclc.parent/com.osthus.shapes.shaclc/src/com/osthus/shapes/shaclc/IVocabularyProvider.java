package com.osthus.shapes.shaclc;

import org.eclipse.emf.ecore.resource.Resource;

public interface IVocabularyProvider {

	IVocabulary getVocabulary(Resource resource);
}
