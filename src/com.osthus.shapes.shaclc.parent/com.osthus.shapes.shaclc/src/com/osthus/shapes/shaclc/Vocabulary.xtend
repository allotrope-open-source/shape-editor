package com.osthus.shapes.shaclc

import java.io.IOException
import java.io.InputStream
import java.net.MalformedURLException
import java.net.URL
import java.net.URLConnection
import java.util.Collections
import java.util.HashMap
import java.util.Map
import java.util.Set
import org.apache.jena.graph.Factory
import org.apache.jena.graph.Graph
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.Statement
import org.apache.jena.rdf.model.StmtIterator
import org.apache.jena.riot.RiotException
import org.apache.jena.vocabulary.OWL2
import org.apache.jena.vocabulary.RDFS
import org.apache.jena.vocabulary.SKOS
import org.apache.log4j.Logger
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.URIConverter
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFLanguages
import java.util.concurrent.ConcurrentHashMap

class Vocabulary implements IVocabulary {
	private static final Logger log = Logger.getLogger(typeof(Vocabulary))
	val prefix2NS = new ConcurrentHashMap<String, String>();
	val ns2Prefix = new ConcurrentHashMap<String, String>();
	val iri2Label = new ConcurrentHashMap<String, String>();
	val label2Iri = new ConcurrentHashMap<String, Map<String, String>>();
	val imports = new ConcurrentHashMap<String, String>()
	val URI baseURI
	val ICatalog catalog;
	val org.eclipse.emf.ecore.resource.Resource resource;

	new(org.eclipse.emf.ecore.resource.Resource resource, URI baseURI, ICatalog catalog) {
		this.resource = resource;
		this.baseURI = baseURI;
		this.catalog = catalog
		init
	}

	private def void init() {
		// initial load needed
		for (imp : SHACLCHelper.getImports(resource)) {
			addOntology(imp.toString)
		}
		for (e : SHACLCHelper.getPrefixes(resource).entrySet) {
			addPrefix(e.key, e.value)
		}
	}

	private def static Graph createMemoryGraph() {
		return Factory.createDefaultGraph();
	}

	private def static Model createMemoryModel() {
		return ModelFactory.createModelForGraph(createMemoryGraph());
	}

	override def Set<String> prefixes() {
		return prefix2NS.keySet();
	}

	override def Set<String> ontologies() {
		return imports.keySet();
	}

	override def synchronized boolean addPrefix(String prefix, String ns) {
		var p = if (prefix.endsWith(":")) prefix.substring(prefix.length-1) else prefix;
		if (prefix2NS.containsKey(p) && prefix2NS.get(p) == ns) {
			return false;
		} else {
			log.info("registered " + prefix + " for namespace " + ns)
			this.prefix2NS.put(p, ns);
			this.ns2Prefix.put(ns, p);
			updatePrefixedLabels(ns);
			return true
		}
	}

	override def Set<String> labelsForNamespace(String ns) {
		if (label2Iri.containsKey(ns)) {
			val l2i = label2Iri.get(ns)
			return l2i.keySet
		} else {
			return Collections.emptySet()
		}
	}

	override def Set<String> labelsForPrefix(String prefix) {
		if (prefix2NS.containsKey(prefix)) {
			val ns = prefix2NS.get(prefix)
			return labelsForNamespace(ns)
		} else {
			return Collections.emptySet()
		}
	}

	override def String iriOfPrefixLabel(String prefix, String label) {
		if (prefix2NS.containsKey(prefix)) {
			val ns = prefix2NS.get(prefix)
			return iriOfNamespaceLabel(ns, label, prefix)
		} else {
			log.warn("Undeclared namespace prefix " + prefix);
			return null
		}
	}

	override def String iriOfNamespaceLabel(String ns, String label) {
		return iriOfNamespaceLabel(ns, label, null)
	}

	override def String iriOfNamespaceLabel(String ns, String label, String prefix) {
		if (label2Iri.containsKey(ns)) {
			val l2i = label2Iri.get(ns)
			if (l2i.containsKey(label)) {
				var iri = l2i.get(label)
				if (prefix !== null) {
					return prefix + ":" + iri;
				} else {
					return ns + iri
				}
			} else {
				log.warn("No label '" + label + "' found in namespace <" + ns + ">");
				return null;
			}
		} else {
			log.warn("No vocabulary imported from namespace <" + ns + ">");
			return null;
		}
	}

	override def String iriForPrefixedLabel(String pLabel) {
		var String prefixedLabel
		if (pLabel.startsWith("\u00ab")) {
			prefixedLabel = pLabel.substring(1, pLabel.length - 1)
		} else if (pLabel.startsWith("<<")) {
			prefixedLabel = pLabel.substring(2, pLabel.length - 2)
		}
		val parts = prefixedLabel.split(":", 2);
		val prefix = parts.get(0)
		val label = parts.get(1)
		return iriOfPrefixLabel(prefix, label)
	}

//	private def Model readOntologyModel(URL realUrl, String format) throws RiotException, IOException {
//		var InputStream is;
//		try {
//			var model = createMemoryModel();
//			model.read(realUrl.toString);
//			return model;
//		} finally {
//			if (is !== null) {
//				is.close();
//			}
//		}
//
//	}

	private def Model readModel(URI ontologyURI) throws IOException, MalformedURLException {
		if (ontologyURI.toString.startsWith("http:") || ontologyURI.toString.startsWith("https:")) {
			return readModelFromWeb(ontologyURI)
		} else {
			return readModelFromFile(ontologyURI)
		}
	}

	private def Model readModelFromWeb(URI ontologyURI) throws IOException, MalformedURLException {
		var tempModel = createMemoryModel();
		var URLConnection connection
		var url = new URL(ontologyURI.toString);
		connection = url.openConnection();

		val StringBuilder acceptHeader = new StringBuilder();
		acceptHeader.append("text/turtle;q=1.0")
		acceptHeader.append(", application/x-turtle;q=1.0")
		acceptHeader.append(", application/rdf+xml;q=1.0")
		acceptHeader.append(", text/n3;q=1.0")
		acceptHeader.append(", text/plain;q=0.5") // NTRIPLE
		connection.addRequestProperty("Accept", acceptHeader.toString());

		var contentType = connection.getContentType();

		// Remove everything after a semicolon. It could be charset information
		var splitIndex = contentType.indexOf(";");
		if (splitIndex > 0) {
			contentType = contentType.substring(0, splitIndex);
		}

		var String fileSuffix = ontologyURI.fileExtension
		var Lang lang = null;
		switch (contentType) {
			case "text/turtle",
			case "application/x-turtle":
				lang = RDFLanguages.TURTLE
			case "application/rdf+xml":
				lang = RDFLanguages.RDFXML
			case "text/n3":
				lang = RDFLanguages.N3
			default:
				switch (fileSuffix) {
					case "ttl",
					case "turtle":
						lang = RDFLanguages.TURTLE
					case "rdf",
					case "xml":
						lang = RDFLanguages.RDFXML
					case "n3":
						lang = RDFLanguages.N3
					case "nt":
						lang = RDFLanguages.NTRIPLES
					default: lang=RDFLanguages.TURTLE
				}
		}
		log.info("Loading ontology from URI " + ontologyURI + "in " + lang + " format")
		RDFDataMgr.read(tempModel, connection.getInputStream(), lang)

		return tempModel;
	}

	private def Model readModelFromFile(URI ontologyURI) throws IOException {
		val ResourceSet resourceSet = resource.resourceSet
		val URIConverter uric = resourceSet.URIConverter
		var Lang lang = null
		var String fileSuffix = ontologyURI.fileExtension
		switch (fileSuffix) {
			case "ttl",
			case "turtle":
				lang = RDFLanguages.TURTLE
			case "rdf",
			case "xml":
				lang =  RDFLanguages.RDFXML
			case "n3":
				lang = RDFLanguages.N3
			case "nt":
				lang = RDFLanguages.NTRIPLES
			default:
				lang = RDFLanguages.TURTLE // default if no fileextension
		}
		val tempModel = createMemoryModel();
		var InputStream is = null

		log.info("Loading ontology from URI " + ontologyURI + "in " + lang + " format")
		try {
			is = uric.createInputStream(ontologyURI);
			RDFDataMgr.read(tempModel, is, lang)

		} finally {
			if (is !== null)
				is.close()
		}
		return tempModel;
	}

	override def synchronized void reload() {
		this.iri2Label.clear
		this.label2Iri.clear
		this.ns2Prefix.clear
		this.prefix2NS.clear
		this.imports.clear
		init

	}

	// returns true if added
	override def synchronized boolean addOntology(String url) {
		if (! imports.containsKey(url)) {
			var realURI = catalog.replacementOf(URI.createURI(url))
			var Model model;
			try {
				model = readModel(realURI)
			} catch (MalformedURLException e1) {
				imports.put(url, "<urn:bad>");
				log.error(e1);
				throw new RuntimeException("Ontology URL <" + url + "> mapped to malformed URL " + realURI, e1);
			} catch (IOException e2) {
				imports.put(url, "<urn:bad>");
				log.error(e2);
				throw new RuntimeException(
					"Cannot read ontology <" + url + "> from location " + realURI + "\n" + e2.message, e2);
			} catch (RiotException e3) {
				imports.put(url, "<urn:bad>");
				log.error(e3);
				throw new RuntimeException(
					"Cannot read ontology <" + url + "> from location " + realURI + "\n" + e3.message, e3);
			}
			if (model === null) {
				imports.put(url, "<urn:bad>");
				throw new RuntimeException("Cannot read ontology <" + url + "> from location " + realURI);

			}
			// nestedRead(model);
			var iter = model.listStatements(null as Resource, SKOS.prefLabel, null as String)
			readLabels(iter)
			iter = model.listStatements(null as Resource, RDFS.label, null as String)
			readLabels(iter)
			imports.put(url, realURI.toString)
			var nit = model.listObjectsOfProperty(OWL2.imports)
			while (nit.hasNext) {
				var node = nit.next
				var import = node.asResource.URI
				addOntology(import)
			}
			updatePrefixedLabels
			return true
		} else {
			return false
		}
	}

	protected def void readLabels(StmtIterator iter) {
		while (iter.hasNext()) {
			var stmt = iter.next as Statement;
			var subj = stmt.subject;
			var obj = stmt.object;
			var label = obj.asLiteral;
			var uri = subj.URI;
			iri2Label.put(uri, label.toString);
		}
	}

	def void updatePrefixedLabels() {
		for (String pns : ns2Prefix.keySet) {
			updatePrefixedLabels(pns);
		}
	}

	protected def updatePrefixedLabels(String pns) {
		for (uri : iri2Label.keySet) {
			if (uri.startsWith(pns)) {
				var localName = uri.substring(pns.length);
				var l2i = label2Iri.get(pns)
				if (l2i === null) {
					l2i = new HashMap<String, String>()
					label2Iri.put(pns, l2i)
				}
				var label = iri2Label.get(uri)
				l2i.put(label.toString, localName);
			}
		}

	}
}
