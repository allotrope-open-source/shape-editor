package com.osthus.shapes.shaclc;

import java.util.Set;

import org.eclipse.emf.common.util.URI;

public interface ICatalog {

	URI replacementOf(URI uri);

	Set<URI> catalogIRIs();

	void reload();
}
