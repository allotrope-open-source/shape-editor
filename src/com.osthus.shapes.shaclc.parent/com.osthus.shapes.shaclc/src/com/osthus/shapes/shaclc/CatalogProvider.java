package com.osthus.shapes.shaclc;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;

import com.google.inject.Singleton;

@Singleton
public class CatalogProvider implements ICatalogProvider {
	private static final Logger log = Logger.getLogger(CatalogProvider.class);
	private static String CATALOG_FILE = "catalog-v001.xml";
	private Map<String, ICatalog> catalogs = new HashMap<>();

	@Override
	public ICatalog getCatalog(Resource resource) {
		String resourceURI = resource.getURI().toString();
		ICatalog cat = catalogs.get(resourceURI);
		if (cat == null) {
			ResourceSet resourceSet = resource.getResourceSet();
			URIConverter uric = resourceSet.getURIConverter();
			URI folder = resource.getURI().trimSegments(1);
			URI catalogFile = folder.appendSegment(CATALOG_FILE);

			Catalog catalog = new Catalog(catalogFile, uric);
			if (catalog.exists()) {
				try {
					catalog.reload();
					cat = catalog;
				} catch (Exception e) {
					log.warn("Error reading catalog " + catalogFile, e);
					cat = new NoopCatalog();
				}
			} else {
				log.warn("Catalog file " + catalogFile + " not found.");
				cat = new NoopCatalog();
			}
			catalogs.put(resourceURI, cat);
		}
		return cat;
	}
}
