package com.osthus.shapes.shaclc

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.util.HashMap
import java.util.Map
import java.util.Set
import javax.xml.parsers.DocumentBuilderFactory
import org.apache.log4j.Logger
import org.eclipse.emf.common.util.URI;
import org.w3c.dom.Element
import org.eclipse.emf.ecore.resource.URIConverter
import java.util.concurrent.ConcurrentHashMap

class Catalog implements ICatalog {
	private static final Logger log = Logger.getLogger(typeof(Catalog))
	val URI catalogFile;
	val URIConverter uriConverter;
	var Map<URI, URI> mapping = new ConcurrentHashMap<URI, URI>()

	new(URI catalogFile, URIConverter uriConverter) {
		this.catalogFile = catalogFile;
		this.uriConverter = uriConverter;
		reload
	}

	// without eclipse
	new(String folder, String filename) {
		this.catalogFile = URI.createFileURI(folder + filename)
		this.uriConverter = null;
		reload
	}

	def boolean exists() {
		if (uriConverter !== null) {
			return uriConverter.exists(catalogFile, null)
		} else {
			if (catalogFile.file) {
				val file = new File(catalogFile.path)
				if (file.exists && file.isFile) {
					return true
				} else {
					log.warn("Catalog file does not exist at " + file.absolutePath)
					return false
				}
			} else if (catalogFile.platformResource) {
				var path = catalogFile.toPlatformString(true);
				val platformPrefix = "platform:/resource/"
				path = path.substring(path.indexOf('/', platformPrefix.length) + 1);
				val file = new File(path)
				if (file.exists && file.isFile) {
					return true
				} else {
					log.warn("Catalog file does not exist at " + file.absolutePath)
					return false
				}
			} else {
				false;
			}
		}
	}

	override def synchronized void reload() {
		mapping.clear
		var InputStream is = null
		if (uriConverter !== null) {
			is = uriConverter.createInputStream(catalogFile)
		} else {
			log.warn("reloading without uri converter from " + catalogFile)
			if (catalogFile.file) {
				is = new FileInputStream(catalogFile.toFileString)
			} else if (catalogFile.platformResource) {
				var path = catalogFile.toPlatformString(true);
				val platformPrefix = "platform:/resource/"
				path = path.substring(path.indexOf('/', platformPrefix.length) + 1);
				is = new FileInputStream(path);
			}
		}
		if (is !== null) {
			try {
				mapping = loadCatalog(is)
			} catch (IOException e) {
				log.error(e);
			} finally {
				if (is !== null)
					is.close
			}
		} else {
			log.error("Cannot reload catalog file " + catalogFile)
		}
	}

	private def Map<URI, URI> loadCatalog(InputStream is) throws IOException {
		var dbFactory = DocumentBuilderFactory.newInstance();
		var dBuilder = dbFactory.newDocumentBuilder();
		var catalog = dBuilder.parse(is)
		var map = new HashMap<URI, URI>()
		var uriMappings = catalog.getElementsByTagName("uri");
		for (var i = 0; i < uriMappings.getLength(); i++) {
			var uriMapping = uriMappings.item(i) as Element;
			var URI origURI = URI.createURI(uriMapping.getAttribute("name"))
			var URI replURI = URI.createURI(uriMapping.getAttribute("uri"))
			if (replURI.relative) {
				var resolveURI = replURI.resolve(catalogFile, false);
				map.put(origURI, resolveURI);
			} else {
				map.put(origURI, replURI);
			}
		}
		return map;
	}

	override def Set<URI> catalogIRIs() {
		return mapping.keySet();
	}

	override def URI replacementOf(URI uri) {
		if (mapping.containsKey(uri)) {
			return mapping.get(uri)
		} else
			return uri;
	}

}
