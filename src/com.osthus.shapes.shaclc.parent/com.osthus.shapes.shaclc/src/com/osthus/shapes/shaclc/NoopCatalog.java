package com.osthus.shapes.shaclc;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.common.util.URI;

public class NoopCatalog implements ICatalog {
	@Override
	public Set<URI> catalogIRIs() {
		return Collections.emptySet();
	}

	@Override
	public URI replacementOf(URI url) {
		return url;
	}

	@Override
	public void reload() {
	}
}
