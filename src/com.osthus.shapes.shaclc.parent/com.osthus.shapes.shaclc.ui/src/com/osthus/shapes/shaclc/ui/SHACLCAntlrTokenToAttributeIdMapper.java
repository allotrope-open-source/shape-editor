package com.osthus.shapes.shaclc.ui;

import java.util.HashSet;
import java.util.Set;

import org.antlr.runtime.Token;
import org.eclipse.xtext.ide.editor.syntaxcoloring.AbstractAntlrTokenToAttributeIdMapper;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;

import com.google.inject.Singleton;

@Singleton
public class SHACLCAntlrTokenToAttributeIdMapper extends AbstractAntlrTokenToAttributeIdMapper {

	@Override
	protected String calculateId(String tokenName, int tokenType) {
		if (tokenType == Token.INVALID_TOKEN_TYPE) {
			return DefaultHighlightingConfiguration.INVALID_TOKEN_ID;
		}
		switch (tokenName) {
		case "RULE_COMMENT":
			return DefaultHighlightingConfiguration.COMMENT_ID;
		case "RULE_DECIMAL":
		case "RULE_DOUBLE":
		case "RULE_INTEGER":
			return DefaultHighlightingConfiguration.NUMBER_ID;
		case "RULE_STRING_LITERAL1":
		case "RULE_STRING_LITERAL_LONG1":
		case "RULE_STRING_LITERAL2":
		case "RULE_STRING_LITERAL_LONG2":
			return DefaultHighlightingConfiguration.STRING_ID;
		default:
			if (keywords.contains(tokenName)) {
				return DefaultHighlightingConfiguration.KEYWORD_ID;
			} else if (tokenName.startsWith("RULE_OP_")) {
				return DefaultHighlightingConfiguration.KEYWORD_ID;
			} else if (sparqlKeywords.contains(tokenName.toLowerCase())) {
				return DefaultHighlightingConfiguration.KEYWORD_ID;
			} else if (sparqlBuiltins.contains(tokenName.toUpperCase())) {
				return DefaultHighlightingConfiguration.DEFAULT_ID;
			} else {
				return DefaultHighlightingConfiguration.DEFAULT_ID;
			}

		}
	}

	private static Set<String> setOf(String... str) {
		HashSet<String> set = new HashSet<String>();
		for (int i = 0; i < str.length; ++i) {
			set.add(str[i]);
		}
		return set;
	}

	private static Set<String> sparqlKeywords = setOf("'as'", "'ask'", "'asc'", "'bind'", "'by'", "'construct'",
			"'desc'", "'distinct'", "'exists'", "'filter'", "'graph'", "'group'", "'having'", "'in'", "'not'",
			"'optional'", "'order'", "'reduced'", "'select'", "'union'", "'where'");
	private static Set<String> sparqlBuiltins = setOf("'STR'", "'LANG'", "'LANGMATCHES'", "'DATATYPE'", "'BOUND'",
			"'IRI'", "'URI'", "'BNODE'", "'RAND'", "'CEIL'", "'FLOOR'", "'ROUND'", "'CONCAT'", "'SUBSTR'", "'STRLEN'",
			"'REPLACE'", "'UCASE'", "'LCASE'", "'ENCODE_FOR_URI'", "'CONTAINS'", "'STRSTARTS'", "'STRENDS'",
			"'STRBEFORE'", "'STRAFTER'", "'YEAR'", "'MONTH'", "'DAY'", "'HOURS'", "'MINUTES'", "'SECONDS'",
			"'TIMEZONE'", "'TZ'", "'NOW'", "'UUID'", "'STRUUID'", "'MD5'", "'SHA1'", "'SHA256'", "'SHA384'", "'SHA512'",
			"'COALESCE'", "'IF'", "'STRLANG'", "'STRDT'", "'SAMETERM'", "'ISIRI'", "'ISURI'", "'ISBLANK'",
			"'ISLITERAL'", "'REGEX'", "'COUNT'", "'SUM'", "'MIN'", "'MAX'", "'AVG'", "'SAMPLE'", "'GROUP_CONCAT'",
			"'SEPARATOR'");
	private static Set<String> keywords = setOf("'BASE'", "'BlankNode'", "'BlankNodeOrIRI'", "'BlankNodeOrLiteral'",
			"'IMPORTS'", "'IRI'", "'IRIOrLiteral'", "'Literal'", "'PREFIX'", "'VERSION'", "'LIBRARY'", "'and'",
			"'class'", "'closed'", "'defaultValue'", "'description'", "'deactivated'", "'false'", "'function", "'hasValue'",
			"'if'", "'in'", "'maxExclusive'", "'maxInclusive'", "'maxLength'", "'message'", "'minExclusive'",
			"'minInclusive'", "'minLength'", "'name'", "'not'", "'optional'", "'or'", "'order'", "'pattern'",
			"'qualified'", "'returns'", "'rule'", "'parameter'", "'priority'", "'property'",  "'severity'", "'shape'",
			"'shapeClass'", "'target'",	"'true'", "'validator'", "'xone'", "'xor'");
}
