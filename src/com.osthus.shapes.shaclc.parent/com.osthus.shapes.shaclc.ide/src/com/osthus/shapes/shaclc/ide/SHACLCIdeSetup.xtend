/*
 * generated by Xtext 2.13.0
 */
package com.osthus.shapes.shaclc.ide

import com.google.inject.Guice
import com.osthus.shapes.shaclc.SHACLCRuntimeModule
import com.osthus.shapes.shaclc.SHACLCStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class SHACLCIdeSetup extends SHACLCStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new SHACLCRuntimeModule, new SHACLCIdeModule))
	}
	
}
